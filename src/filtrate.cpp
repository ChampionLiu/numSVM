#include "filtrate.h"

bool NumFiltrate::pretreat(Mat InputArray, Mat& OutputArray)
{
	//judge channels of Mat
	if(InputArray.channels() == 3)
		cvtColor(InputArray, imgTmp, CV_BGR2GRAY);
	else if(InputArray.channels() == 1)
		imgTmp = InputArray;
	else
		return false;

	imgROI = InputArray.clone();

	threshold(imgTmp, imgTmp, 0, 255, CV_THRESH_OTSU + CV_THRESH_BINARY);

	Mat element = getStructuringElement(MORPH_ELLIPSE, Size(3,3), Point(1,1));
	erode(imgTmp, imgDst, element);

	imshow("pretreat dst", imgDst);
	waitKey(0);

	OutputArray = imgDst;

	return true;
}

bool NumFiltrate::split(Mat InputArray, vector<Mat>& OutputArrays)
{
	vector< vector<Point> > contours;
	findContours(InputArray, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

	vector< vector<Point> >::iterator it = contours.begin();
	while(it != contours.end())
	{
		RotatedRect rect = minAreaRect(Mat(*it));
		if(verifyRect(rect))
		{
			++it;
		}
		else
		{
			it = contours.erase(it);
		}
	}


	vector<Rect> boundRects(contours.size());
	for(size_t i = 0; i < contours.size(); ++i)
	{
		Scalar color = Scalar(100,100,100);
		boundRects[i] = boundingRect(Mat(contours[i]));
		rectangle(imgROI, boundRects[i].tl(), boundRects[i].br(), color, 0.5, 8, 0);

		Rect roi = Rect(boundRects[i]);	
		OutputArrays.push_back(imgTmp(roi));	
	}

	imshow("extract num", imgROI);
	waitKey(0);

	return true;
}

bool NumFiltrate::pretreatSplit(Mat InputArray, vector<Mat>& OutputArrays)
{
	if(!pretreat(InputArray, imgDst))
		return false;
	if(!split(imgDst, OutputArrays))
		return false;
	return true;
}

bool NumFiltrate::normalize(vector<Mat>& InputArrays, Size imgSize)
{
	for(size_t i = 0; i < InputArrays.size(); i++)
	{
		resize(InputArrays[i], InputArrays[i], imgSize);
	}
	return true;
}

bool NumFiltrate::verifyRect(RotatedRect rect)
{
	
	float ratio = rect.size.width / rect.size.height;
	cout << "ratio = " << ratio << endl;
	if((ratio >= 0.4) && (ratio <= 17.5))
		return true;
	else
		return false;
	
	return true;
}
