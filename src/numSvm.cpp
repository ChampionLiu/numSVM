#include "numSvm.h"

bool NumSVM::init(CvSVMParams svm_params)
{
	params = svm_params;
	return true;
}

bool NumSVM::train(vector<Mat> InputArrays, vector<int> InputLabels)
{
	if((InputArrays.size() != InputLabels.size()) || (InputArrays.size() == 0))
		return false;

	Mat imgTmp;
	size_t total = InputArrays.size();
	for(size_t i = 0; i < total; i++)
	{
		imgTmp = InputArrays[i].reshape(1,1);
		trainingData.push_back(imgTmp);
		trainingLabels.push_back(InputLabels[i]);
	}

	//Mat trainingLabelsMat(matRows, InputArrays[0].cols, CV_32FC1, trainingLabels);
	//Mat trainingDataMat(matRows, 1, CV_32FC1, trainingData);
	Mat(trainingLabels).copyTo(svmLabel);
	Mat(trainingData).copyTo(svmImage);
	svmImage.convertTo(svmImage, CV_32FC1);

	//SVM.train(trainingDataMat, trainingLabelsMat, Mat(), Mat(), params);
	SVM.train(svmImage, svmLabel, Mat(), Mat(), params);

	FileStorage fs("./outputs/numSVM.xml", FileStorage::WRITE);
	fs << "TrainingData" << svmImage;
	fs << "TrainingLabels" << svmLabel;
	fs.release();

	return true;
}

bool NumSVM::predict()
{
	return true;
}
