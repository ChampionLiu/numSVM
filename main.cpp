#include "filtrate.h"
#include "numSvm.h"
#include <string>
#include <stdio.h>
#include <unistd.h>

int str2char(string s, char c[])
{
	int l = s.length();
	int i;
	for(i = 0; i < l; i++)
	{
		c[i] = s[i];
	}
	c[i] = '\0';
	return i;
}

int main(int argc, char** argv)
{
	string cmd;
	cout << " [ Welcome to use numSVM ]" << endl;
	cout << "< Authored by Champion Liu >" << endl;
	cout << "***This is a Console Demo***" << endl;

	bool isQuit = false;
	
	while(!isQuit)
	{
		
		cout << ">>>";
		cin >> cmd;

		if(cmd == "demo")
		{
			NumFiltrate numFilter;

			Mat imgSmp = imread("./sample/sample_1.png");
			Mat imgDst;
			vector<Mat> outputs;

			imshow("origin", imgSmp);
			numFilter.pretreat(imgSmp, imgDst);
			numFilter.split(imgDst, outputs);
			numFilter.normalize(outputs, Size(8,16));

			for(size_t i = 0; i < outputs.size(); i++)
			{
				string caption = "exact num" + to_string(i);
				string filename = "./numlib/numlib_" + to_string(i) + ".jpg";
				imwrite(filename, outputs.at(i));
				imshow(caption, outputs.at(i));
			}


			vector<int> labels;
			labels.push_back(2);
			labels.push_back(11);
			labels.push_back(2);
			labels.push_back(2);
			labels.push_back(2);

			NumSVM numsvm;
			if(numsvm.train(outputs, labels))
				cout << "training done" << endl;

			waitKey(0);
		}
		else if(cmd == "filtrate")
		{
			cout << ">>>>>>Input training num: ";
			string proname;
			cin >> proname;

			cout << ">>>>>>Input filename: ";
			string filename;
			cin >> filename;
			Mat imgInput = imread("./sample/" + filename);

			NumFiltrate numFilter;
			vector<Mat> outputs;

			numFilter.pretreatSplit(imgInput, outputs);
			numFilter.normalize(outputs, Size(8,16));

			int j = -1;
			char dirfiles[255];
			do
			{
				j++;
				string files = "./numlib/" + proname + "/" + to_string(j) + ".jpg";
				str2char(files, dirfiles);
			}while((access(dirfiles, R_OK|W_OK) == 0) && (j <= 1000));

			for(size_t i = 0; i < outputs.size(); i++)
			{
				string caption = "exact num" + to_string(i);
				string filename = "./numlib/" + proname + "/" + to_string(i + j) + ".jpg";
				imwrite(filename, outputs.at(i));
				imshow(caption, outputs.at(i));
			}
		}
		else if(cmd == "train")
		{

		}
		else if(cmd == "predict")
		{
			cout << ">>>>>>please input file name: ";
			string path;
			cin >> path;
			
			Mat pic = imread(path);
			if(pic.channels() == 3)
				cvtColor(pic, pic, CV_BGR2GRAY);

			imshow("pic", pic);
			waitKey(0);

			resize(pic, pic, Size(8,16));
			Mat svmPic = pic.reshape(1,1);
			svmPic.convertTo(svmPic, CV_32FC1);

			FileStorage fs;
			fs.open("./outputs/numSVM.xml", FileStorage::READ);
			Mat svmData, svmLabel;
			fs["TrainingData"] >> svmData;
			fs["TrainingLabels"] >> svmLabel;
			
			CvSVMParams params;
			params.svm_type = CvSVM::C_SVC;
			params.kernel_type = CvSVM::LINEAR;
			params.term_crit = cvTermCriteria(CV_TERMCRIT_ITER, 100, 1E-6);
			CvSVM svmClassifier(svmData, svmLabel, Mat(), Mat(), params);

			int response = (int)svmClassifier.predict(svmPic);
			cout << "Picture: " << path << endl << "Result: " << response << endl;
		}
		else if(cmd == "help")
		{
			cout << "[help desk]" << endl;
			cout << "- help<enter> | ask for help desk\n"
				 << "- filtrate<enter> | pretreat input image and split\n"
				 << "- train<enter> | train SVM by the input samples\n"
				 << "- predict<enter> | use SVM to predict target\n"
				 << "- exit<enter> | quit the program\n";
		}
		else if(cmd == "exit")
		{
			cout << "good bye" << endl;
			isQuit = true;
		}
	}
	return 0;
}
