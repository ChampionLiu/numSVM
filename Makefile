TARGET = numSVM

SRCS := $(wildcard src/*.cpp ./*.cpp)

OBJ := $(patsubst %cpp,%o,$(SRCS))

CFLAGS = -g -Wall -I/usr/local/include -Iinc/ -std=c++11

LDFLAGS = -Wl,-rpath,./ `pkg-config --cflags --libs opencv`

CXX = g++

$(TARGET) : $(OBJ)
	$(CXX) -o $(TARGET) $(OBJ) $(LDFLAGS)

%.o:%.cpp
	$(CXX) $(CFLAGS) -c $< -o $@

clean:
	rm src/*.o
