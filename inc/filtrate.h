#ifndef _FILTRATE_H
#define _FILTRATE_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <vector>

using namespace cv;
using namespace std;

class NumFiltrate
{
public:
	NumFiltrate()
	{
	}
	NumFiltrate(Mat InputArray, Rect ROI)
	{
		imgROI = InputArray(ROI);
	}

	~NumFiltrate()
	{
	}

	bool pretreat(Mat InputArray, Mat& OutputArray);
	bool split(Mat InputArray, vector<Mat>& OutputArrays);
	bool pretreatSplit(Mat InputArray, vector<Mat>& OutputArrays);
	bool normalize(vector<Mat>& InputArrays, Size imgSize);

private:
	Mat
		imgROI,
		imgTmp,
		imgDst;

	bool verifyRect(RotatedRect rect);
};

#endif

