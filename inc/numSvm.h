#ifndef __NUMSVM_H
#define __NUMSVM_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>
#include <iostream>
#include <vector>
#include <fstream>

using namespace cv;
using namespace std;

class NumSVM
{
public:
	NumSVM()
	{
		params.svm_type = CvSVM::C_SVC;
		params.kernel_type = CvSVM::LINEAR;
		params.term_crit = cvTermCriteria(CV_TERMCRIT_ITER, 100, 1E-6);
	}

	~NumSVM()
	{
	}

	bool init(CvSVMParams svm_params);
	bool train(vector<Mat> InputArrays, vector<int> InputLabels);
	bool predict();

private:
	vector<int> trainingLabels;
	Mat trainingData;

	Mat svmLabel, svmImage;

	CvSVMParams params;
	CvSVM SVM;

};

#endif

